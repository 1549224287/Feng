package com.feng.core;

/**
 * @Auther: Xavier
 * @Date: 2018/8/15 21:54
 * @Description:
 */
public interface IExceptionDefined {
    /**
     * 异常代码
     * @return
     */
    String getCode();


    /**
     * 异常信息
     * @return
     */
    String getMessage();

}
