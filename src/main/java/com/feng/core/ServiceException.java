package com.feng.core;

/**
 * @Auther: Xavier
 * @Date: 2018/8/15 22:05
 * @Description:
 */
public class ServiceException extends Exception {

    private IExceptionDefined exception;

    public ServiceException(IExceptionDefined exception) {
        this.exception=exception;
    }

    public IExceptionDefined getException(){
        return exception;
    }

}
