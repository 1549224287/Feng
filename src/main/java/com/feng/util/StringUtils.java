package com.feng.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;

/**
 * @Auther: Xavier
 * @Date: 2018/8/14 23:23
 * @Description:
 */
public class StringUtils {
    /**
     * uuid 生成
     * @return
     */
    public static String uuid(){
        return UUID.randomUUID().toString().replace("-", "");
    }
    /**
     * 判断字符串是否为空
     * @param str
     * @return
     */
    public static boolean checkNull(String str){
        return (null == str) || ("".equals(str)) ? false : true;
    }

    /**
     * 检查对象是否为空
     * @param obj
     * @return
     */
    public static boolean checkNull(Object obj){
        if (null == obj)
            return true;
        else if (obj instanceof String)
            return "".equals(((String) obj).trim()) || "null".equals(((String) obj).trim());
        else if (obj instanceof Collection)
            return ((Collection) obj).size() == 0;
        else if (obj instanceof Map)
            return  ((Map) obj).size() == 0;
        return false;
    }

    //2个字符串是否相等
    public static boolean checkEq(String str,String atr){
        return str.equals(atr) ? true : false;
    }

    //获取字符串某个字符的字数
    public static int getStrSize(String str){
        char [] cr =  str.toCharArray();
        int num = 0;
        for (int i=0;i<cr.length;i++){
            if (i==cr[i])
                num++;
        }
        return num;
    }

    //检查map是否有效
    public static boolean checkMap(Map map){
        return (null == map) || (map.isEmpty());
    }

    //判断一组Map 是否有效
    public static boolean checkMap(Map ... maps){
        for (Map map : maps){
            if (checkMap(map))
                return false;
        }
        return true;
    }

    /**
     * 加密MD5
     * @param str
     * @return
     */
    public static String encryption(String str){
        return encryption(str, "MD5");
    }

    /**
     * 加密
     * @param str
     * @param type (MD5/SHA1)
     * @return
     */
    public static String encryption(String str,String type){
        try {
            MessageDigest digest = MessageDigest.getInstance(type);
            digest.update(str.getBytes());
            byte[] data = digest.digest();
            StringBuilder code = new StringBuilder();
            for (byte b: data)
                code.append(String.format("%02X",b));
            return code.toString();
        }catch (NoSuchAlgorithmException e){
            return str;
        }
    }

    /**
     * 检查是否为手机号码
     * @param phone
     * @return
     */
    public static boolean isPhone(String phone){
        if (checkNull(phone))
            return false;
        return phone.matches("1\\d{10}");
    }



    public static void main(String[] args) {
        String uuid = uuid();
        System.out.println(uuid);
    }

}
