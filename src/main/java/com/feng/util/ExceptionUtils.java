package com.feng.util;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @Auther: Xavier
 * @Date: 2018/8/14 23:22
 * @Description:
 */
public class ExceptionUtils {
    /**
     * @param e 异常信息
     * @param packageName 之转换某个包中的信息
     * @return
     */
    public static String stackTranceToString(Throwable e,String packageName){
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw,true));
        String str = sw.toString();
        if (null == packageName)
            return str;
        String [] arrs = str.split("\n");
        StringBuffer subf = new StringBuffer();
        subf.append(arrs[0]+"\n");
        for (int i=0;i<arrs.length;i++){
            String temp = arrs[i];
            if (null != temp && temp.indexOf(packageName) > 0)
                subf.append(temp+"\n");
        }
        return subf.toString();
    }

    /**
     *
     * @param e 异常信息
     * @return
     */
    public static String stackTraceToString(Throwable e){
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw,true));
        return sw.toString();
    }
}
