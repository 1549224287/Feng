package com.feng.core;

/**
 * @Auther: Xavier
 * @Date: 2018/8/15 21:53
 * @Description:
 */
public enum CommonException implements IExceptionDefined {

    请先登录("E-C-00001"),
    参数缺失("E-C-00002");

    private String code;

    private CommonException(String code){this.code=code;}

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return name();
    }
}
