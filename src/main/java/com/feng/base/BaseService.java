package com.feng.base;

import com.feng.core.CommonException;
import com.feng.core.IExceptionDefined;
import com.feng.core.ServiceException;
import com.feng.util.StringUtils;

/**
 * @Auther: Xavier
 * @Date: 2018/8/15 22:26
 * @Description: Service 基类
 */
public abstract class BaseService  {

    /**
     * 参数空项检测
     * @param obj 被检测对象
     * @param fields 对象中属性
     * @param exceptions  异常提示信息
     * @throws Exception
     */
    protected void checkNull(Object obj , String [] fields, IExceptionDefined ... exceptions) throws Exception{
        if (null == obj)
            throw new ServiceException(CommonException.参数缺失);
        Class cls = obj.getClass();
        int i = 0;
        for (String field : fields ){
            String methodName = "get" + field.substring(0,1).toUpperCase() + field.substring(1);
            Object val = cls.getMethod(methodName).invoke(obj);
            if (StringUtils.checkNull(val)){
                throw new ServiceException(exceptions.length>i?exceptions[i]:CommonException.参数缺失);
            }
            i++;
        }
    }

}
