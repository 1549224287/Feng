package com.feng.base;

import com.feng.core.Result;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Auther: Xavier
 * @Date: 2018/8/15 22:10
 * @Description: Controller 基类
 */
public class BaseController {
    /**
     * 异常处理
     * @param e
     * @return
     */
    @ExceptionHandler
    public Result exceptionHandler(Exception e){
        e.printStackTrace();
        return Result.fail(e);
    }

    /**
     * 获取request
     * @return
     */
    protected HttpServletRequest getRequest(){
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    /**
     * 获取reponse
     * @return
     */
    protected HttpServletResponse getResponse(){
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
    }

}
