package com.feng.core;

/**
 * @Auther: Xavier
 * @Date: 2018/8/15 21:59
 * @Description: JSON 封装
 */
public class Result {

    private String code;

    private String message;

    private Object data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public static Result success(){
        return success(null,null);
    }

    public static Result success(Object data){
        return success(null,data);
    }

    public static Result success(String message,Object data){
        Result result = new Result();
        result.code = "0000";
        result.message = null == message ? "操作成功" : message;
        if (null != data)
            result.data = data;
        return result;
    }

    public static Result fail(){
        return fail(null,null);
    }

    public static Result fail(String message){
        return fail(message,null);
    }

    public static Result fail(Exception e){
        return fail(null,e);
    }

    public static Result fail(String message,Exception e){
        Result result = new Result();
        if ( e instanceof ServiceException){
            IExceptionDefined exception = ((ServiceException) e).getException();
            result.code = exception.getCode();
            result.message = exception.getMessage();
        }else {
            result.code = "E-9999";
            result.message = null == message ? "操作失败" : message;
            if ( null != e )
                result.message = e.getLocalizedMessage();
        }
        return result;
    }

}
