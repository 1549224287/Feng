package com.feng.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Auther: Xavier
 * @Date: 2018/8/14 23:24
 * @Description: 日志记录
 */
public class AccessInterceptor implements HandlerInterceptor {

    private static final Logger log = LoggerFactory.getLogger(AccessInterceptor.class);

    private static final ThreadLocal<Long> time = new ThreadLocal<Long>();

    private static final String ACCESSTOKEN = "accessToken";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
            String uri = request.getRequestURI().substring(1);

            //todo  权限验证

            time.set(System.currentTimeMillis());
            String ip = request.getRemoteHost();
            String info = ip + "访问接口" + uri;
            log.info(info);
            return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        String ip = request.getRemoteHost();
        String info = ip + "访问接口" + request.getRequestURI() + ", 耗时=" + (System.currentTimeMillis() - time.get()) + "毫秒";
        log.info(info);
    }
}
