package com.feng.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Auther: Xavier
 * @Date: 2018/8/14 22:58
 * @Description:
 */
public class DateUtils {
    /**
     * 构造函数
     */
    public DateUtils() {
        throw new RuntimeException("this is a utils,can not interface");
    }

    public static final String ENUM_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String ENUM_FORMAT_YDM = "yyyy-MM-dd";

    public static final String ENUM_FORMAT_YDMS = "yyyy-MM-dd HH:mm:ss.S";

    public static final String ENUM_FORMAT_SLASH = "yyyy/MM/dd HH/mm/ss";

    public static final String ENUM_FORMAT_YDM_SLASH = "yyyy/MM/dd HH:mm:ss.S";

    public static final String LEVEL_DAY = "day";

    public static final String LEVEL_HOUR = "hour";

    public static final String LEVEL_MINUTE= "minute";

    public static final String LEVEL_SECOND = "second";

    private static ThreadLocal<SimpleDateFormat> threadTime = new ThreadLocal<SimpleDateFormat>();

    private static SimpleDateFormat DateTimeInstance(){
        SimpleDateFormat sdf = threadTime.get();
        if (null == sdf){
            sdf = new SimpleDateFormat(ENUM_FORMAT);
            threadTime.set(sdf);
        }
        return sdf;
    }

    //获取当前时间
    public static String now(){
        return DateTimeInstance().format(new Date());
    }

    /**
     * 将指定时间转String
     * @param date
     * @return
     */
    public static String dateTime(Date date){
        return DateTimeInstance().format(date);
    }

    /**
     * 判断是否在某个时间内
     * @param start
     * @param end
     * @param time
     * @return
     */
    public static boolean checkInDate(Date start,Date end,Date time){
        return time.after(start) && time.before(end);
    }

    /**
     *
     * @param start
     * @param end
     * @param time
     * @return
     * @throws ParseException
     */
    public static boolean checkInDate(String start,String end,Date time)
            throws ParseException {
        return checkInDate(dateTime(DateTimeInstance().parse(start)),dateTime(DateTimeInstance().parse(end)), time);
    }

    /**
     * 判断字符串是否为时间格式
     * @param date
     * @return
     */
    public static boolean isDate(String date){
        try {
            DateTimeInstance().parse(date);
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 获取2时间的差 单位 s/秒
     * @param before
     * @param after
     * @return
     */
    public static int getTimeDifS(Date before,Date after){
        return  (int) ((before.getTime() - after.getTime()) < 0 ? 0 : (before.getTime() - after.getTime()) / 1000 );
    }

    /**
     * 获取2时间差 天 / 小时  / 分钟  /秒
     * @param before
     * @param after
     * @return
     */
    public static String getTimeDif(Date before,Date after){
        Long time = (before.getTime() - after.getTime()) < 0 ? 0 : (before.getTime() - after.getTime()) / 1000;
        Long minutes = time != 0 ? time / 60 : -1;
        Long hours = (minutes < 60) ? 0 : (minutes / 60) + (minutes % 60);
        Long days = (hours < 24) ? 0 : (hours / 24) + (hours % 24);
        return (days+"天 "+hours+"小时 "+minutes+"分 "+time+"秒");
    }
}
