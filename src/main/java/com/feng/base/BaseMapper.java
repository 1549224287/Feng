package com.feng.base;

import java.io.Serializable;
import java.util.List;

/**
 * @Auther: Xavier
 * @Date: 2018/8/15 22:19
 * @Description: Dao 基类
 */
public interface BaseMapper  {

    /**
     * 保存
     * @param objs
     * @return
     */
    int insert(Object ... objs);

    /**
     * 修改
     * @param obj
     * @return
     */
    int update(Object obj);

    /**
     * 删除
     * @param obj
     * @return
     */
    int delete(Object ... obj);

    /**
     * 读取
     * @param t
     * @param <T>
     * @return
     */
    <T> T load(T t);

    /**
     * 读取所有
     * @param t
     * @param <T>
     * @return
     */
    <T> List<T> listAll(T t);

    /**
     * 根据ID读取
     * @param code
     * @param <T>
     * @return
     */
    <T> T loadByCode(Serializable code);

    /**
     * 根据ID删除
     * @param code
     * @return
     */
    int deleteByCode(Serializable ... code);

    /**
     * 根据条件查询
     * @param t
     * @param <T>
     * @return
     */
    <T> List<T> list(T t);
}
